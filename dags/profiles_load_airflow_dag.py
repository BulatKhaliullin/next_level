import os
import django
import sys

sys.path.append("/Users/bulathaliullin/Documents/git-projects/next_level/")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "next_level.settings")
django.setup()

from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.decorators import dag, task
from yadirect.views import YaDirect, upload_file_to_yc
from yadirect.models import Account


for agency_account in Account.objects.all():
    @dag(dag_id=f"{agency_account.login}_profiles_load_dag", start_date=datetime(2022, 10, 31), schedule=timedelta(hours=6))
    def profiles_load_dag():
        @task
        def load_profiles():
            yd = YaDirect(agency_account.access_token, agency_account.login, agency_account)
            yd.load_profiles()
        load_profiles()
    profiles_load_dag()


    for profile in agency_account.profiles.all():
        dag_id = f"{profile.name}_report_dag"

        @dag(dag_id=dag_id, start_date=datetime(2022, 10, 31), schedule=timedelta(hours=6))
        def dynamic_generated_dag():
            @task
            def load_client_report():
                agency_account = profile.agency_account
                yd = YaDirect(agency_account.access_token, agency_account.login, agency_account)
                out_path = yd.load_report(profile.name)
                #bidder_out_path = yd.load_report_for_ngramm(profile.name)

                if out_path:
                    upload_file_to_yc(out_path, profile.name, 'csv')
                    upload_file_to_yc('res.html', profile.name, 'html')

            load_client_report()

        dynamic_generated_dag()



# with DAG(dag_id='agency_profiles_load',
#          default_args={'depends_on_past': False,
#                        # 'email': ['incidentalname636@gmail.com', ],
#                        # 'email_on_failure': False,
#                        # 'email_on_retry': False,
#                        'retries': 1,
#                        'retry_delay': timedelta(minutes=5)
#                        },
#          description='This dag is loading profiles from YandexDirect agency',
#          schedule=timedelta(days=1),
#          start_date=datetime.now(),
#          catchup=False,
#          tags=['example'],
#          ) as dag:
#     task = PythonOperator(task_id='agency_profiles_load_task', python_callable=load_profiles_dag_generator)
