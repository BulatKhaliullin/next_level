from django.apps import AppConfig


class KpssConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kpss'
