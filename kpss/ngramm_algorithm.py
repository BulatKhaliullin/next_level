import pandas as pd
from .base_algorithm import BaseAlgorithm


class NgrammBidderAlgorithm(BaseAlgorithm):
    aggressive_coefficient = 3
    coefficient = 1.5

    def __init__(self, *args, **kwargs):
        self.account = kwargs.get('account', '')
        self.profile_login = kwargs.get('profile_login', '')
        self.campaign_ids = kwargs.get('campaign_ids', [])
        self.yd = kwargs.get('yd', None)
        super().__init__(*args, **kwargs)

    def build_file(self):
        df = self.get_non_brands_queries(self.df)
        cpa, drr, cpc = self.get_constants(df)
        df['lemmas'], df['nums'] = self.get_normalized(df['Query'])
        lemmas = self.get_by_lemmas(df)
        lemmas = self.get_with_coefficients(lemmas, cpa, drr, cpc)
        fp = open('lemmas.html', 'w', encoding='utf-8')
        fp.write('<meta charset="UTF-8">\n' + lemmas.to_html(index=False, justify='left'))
        keywords_lemmas, keywords_nums = self.get_keywords()
        keywords_df = pd.DataFrame({'lemmas': keywords_lemmas, 'nums': keywords_nums})
        df = self.get_bids(keywords_df, lemmas, cpa, drr)
        df_html = df.to_html(index=False, justify='left')
        f = open('bidder.html', 'w', encoding='utf-8')
        f.write('<meta charset="UTF-8">\n' + df_html)

    def get_bids(self, df, lemmas, cpa, drr):
        cpas, drrs, normalizes = [], [], []
        for i, r in df.iterrows():
            res_cpa, res_drr = 0, 0
            normalizes.append(' '.join([i[0] for i in r['lemmas']] + r['nums']))
            lemmas_len = len(r['lemmas'] + r['nums'])
            for item in [i[0] for i in r['lemmas']] + r['nums']:
                res_cpa += lemmas.get('tcpc_cpa').get(item, cpa * 1.5 * lemmas_len)
                res_drr += lemmas.get('tcpc_drr').get(item, drr * 1.5 * lemmas_len)
            cpas.append(res_cpa / lemmas_len)
            drrs.append(res_drr / lemmas_len)
        return pd.DataFrame({'Нормализованная': normalizes, 'CPA': cpas, 'DRR': drrs})

    def get_by_lemmas(self, df):
        res_dict = {}
        for i, r in df.iterrows():
            for w in [i[0] for i in r['lemmas']] + r['nums']:
                item = res_dict.get(w, {'Word': w, 'Clicks': 0, 'Cost': 0, 'Revenue': 0, 'Conversions': 0, 'Impressions': 0})
                item['Clicks'] += r['Clicks']
                item['Cost'] += r['Cost']
                item['Revenue'] += float(r['Revenue']) if r['Revenue'] != '--' else 0
                item['Conversions'] += float(r['Conversions']) if r['Conversions'] != '--' else 0
                item['Impressions'] += r['Impressions']
                res_dict[w] = item
        return pd.DataFrame(res_dict).transpose()

    def get_constants(self, df):
        cost_sum, conversions_sum, revenue_sum, clicks_sum = 0, 0, 0, 0
        for i, r in df.iterrows():
            cost_sum += float(r['Cost']) if r['Cost'] != '--' else 0
            conversions_sum += float(r['Conversions']) if r['Conversions'] != '--' else 0
            revenue_sum += float(r['Revenue']) if r['Revenue'] != '--' else 0
            clicks_sum += float(r['Clicks']) if r['Clicks'] != '--' else 0
        cpa = cost_sum / conversions_sum if conversions_sum != 0 else 0
        drr = cost_sum / revenue_sum if revenue_sum != 0 else 0
        cpc = cost_sum / clicks_sum if clicks_sum != 0 else 0
        return cpa, drr, cpc

    def get_normalized(self, queries):
        queries = self._delete_stop_word(queries)
        query_nums = self._get_nums(queries)
        queries = self._lemmatize_eng_words(queries)
        query_lemmas = self._lemmatize_rus_words(queries)
        return query_lemmas, query_nums

    def get_non_brands_queries(self, df):
        for i, r in df.iterrows():
            if all(w not in r['CampaignName'] for w in ['Бренд', 'Конкуренты']):
                df = df.drop(i)
        return df

    def get_with_coefficients(self, df, cpa, drr, cpc):
        cpa_list, drr_list, cpc_list, tcpc_cpa_list, tcpc_drr_list = [], [], [], [], []
        for i, r in df.iterrows():
            cur_cpa = r['Cost'] / r['Conversions'] if r['Conversions'] != 0 else 0
            cur_drr = r['Cost'] / r['Revenue'] if r['Revenue'] != 0 else 0
            cur_cpc = r['Cost'] / r['Clicks'] if r['Clicks'] != 0 else 0
            cpa_list.append(cur_cpa)
            drr_list.append(cur_drr)
            cpc_list.append(cur_cpc)
            tcpc_cpa_list.append(cpa / cur_cpa * cur_cpc if cur_cpa != 0 else cpc / self.aggressive_coefficient)
            tcpc_drr_list.append(drr / cur_drr * cur_cpc if cur_drr != 0 else drr / self.aggressive_coefficient)
        df['cpa'] = cpa_list
        df['drr'] = drr_list
        df['cpc'] = cpc_list
        df['tcpc_cpa'] = tcpc_cpa_list
        df['tcpc_drr'] = tcpc_drr_list
        return df

    def get_keywords(self):
        result_keywords = []
        for i in range(0, len(self.campaign_ids), 10):
            offset = 0
            while offset is not None:
                keys_resp = self.yd.keywords().post(data={'method': 'get',
                                                     'params': {'SelectionCriteria': {'CampaignIds': self.campaign_ids[i: i+10]},
                                                                'FieldNames': ['Keyword'],
                                                                'Page': {'Limit': 10000, 'Offset': offset}}})
                result_keywords += [i['Keyword'] for i in keys_resp.data['result']['Keywords']] if 'Keywords' in keys_resp.data['result'] else []
                offset = keys_resp.data['result']['LimitedBy'] if 'LimitedBy' in keys_resp.data['result'] else None
        return self.get_normalized(result_keywords)
