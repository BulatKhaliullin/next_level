from django.contrib import admin
from .models import KPSSAlgorithmResult


@admin.register(KPSSAlgorithmResult)
class KPSSAlgorithmResultAdmin(admin.ModelAdmin):
    pass
