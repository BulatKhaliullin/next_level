from django.urls import path
from .views import *


urlpatterns = [
    path('calculate_coefficient/', KPSSAlgorithmView.as_view(), name='calculate_kpss_coefficient'),
]
