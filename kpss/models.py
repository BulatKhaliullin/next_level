from django.db import models
from .kpss_algorithm import KPSSAlgorithm


class KPSSAlgorithmResult(models.Model):
    filename = models.CharField(max_length=100, default='kpss', verbose_name='Название итогового файла')
    list_name = models.CharField(max_length=100, default='', verbose_name='Название страницы excel')
    query_column_name = models.CharField(max_length=100, default='Query', verbose_name='Название столбца запроса',
                                         help_text='Впишите название столбца запросов в исходном файле')
    key_column_name = models.CharField(max_length=100, default='Key', verbose_name='Название столбца ключа',
                                       help_text='Впишите название столбца ключей в исходном файле')
    file = models.FileField(verbose_name='Исходный файл', upload_to='static/csv_files/%y-%m-%d-%H-%M-%S/')
    file_out = models.FileField(blank=True, null=True, verbose_name='Итоговый файл',
                                upload_to='static/csv_files/')

    def save(self, *args, **kwargs):
        if not self.pk:
            super(KPSSAlgorithmResult, self).save(*args, **kwargs)
            out_path = self.file.path[:self.file.path.rfind('/') + 1] + self.filename + '.csv'
            kpss = KPSSAlgorithm(sheet_name=self.list_name, file_path=self.file, file_out_path=out_path,
                                 query_column_name=self.query_column_name, key_column_name=self.key_column_name)
            kpss.get_coefficient_csv_file()
            self.file_out = out_path[out_path.find('/static') + 1:]
            if 'force_insert' in kwargs:
                kwargs.pop('force_insert')
        super(KPSSAlgorithmResult, self).save(*args, **kwargs)
