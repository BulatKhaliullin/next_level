import re
import pandas as pd
from .stop_words import get_stop_words
import os
import json
from django.conf import settings
from pattern.en import lemma as pattern_lemma


reg_ex = r'[^\da-zA-Zа-яА-ЯӘҒҚҢӨҰҮҺІҖЁёҗәғқңөұүһі ]+'


class BaseAlgorithm():
    mystem_path = os.path.join(settings.BASE_DIR, 'mystem')
    stop_words_path = os.path.join(settings.BASE_DIR, 'stop_words.xlsx')

    def __init__(self, *args, **kwargs):
        self.df = pd.read_csv(kwargs.get('file_path'))
        revenue_name = [i for i in self.df.columns if 'Revenue' in i][0]
        conversions_name = [i for i in self.df.columns if 'Conversions' in i][0]
        self.df.rename(columns={revenue_name: 'Revenue', conversions_name: 'Conversions'}, inplace=True)
        self.stop_words = get_stop_words(file_path=self.stop_words_path)['СТОП-СЛОВА'].tolist()

    def _lemmatize_eng_words(self, sentences_list):
        result_sentences = []
        for sentence in sentences_list:
            new_sentence = []
            for word in sentence.split(' '):
                try:
                    new_sentence.append(pattern_lemma(word))
                except Exception:
                    new_sentence.append(pattern_lemma(word))
            result_sentences.append(' '.join(new_sentence))
        return result_sentences

    def _lemmatize_rus_words(self, sentences_list, step=5000):
        result_lemmas = []
        try:
            for i in range(0, len(sentences_list), step):
                if i + step < len(sentences_list):
                    sentences_string = ' are '.join(sentences_list[i:i + step])
                else:
                    sentences_string = ' are '.join(sentences_list[i:len(sentences_list)])
                stream = os.popen(f'echo "{sentences_string}" | {self.mystem_path} -e UTF-8 --format json')
                cons_res = json.loads(stream.read())

                sentences_lemmas = []
                for item in cons_res:
                    if item['analysis']:
                        sentences_lemmas.append([i['lex'] for i in item['analysis']])
                    elif item['text'] != 'are':
                        sentences_lemmas.append([item['text']])
                    else:
                        result_lemmas.append(sentences_lemmas)
                        sentences_lemmas = []
                result_lemmas.append(sentences_lemmas)
        except Exception as e:
            result_lemmas = self._lemmatize_rus_words(sentences_list, step // 2)
        return result_lemmas

    def _normalize(self, keys, queries):
        keys, queries = self._delete_stop_word(keys), self._delete_stop_word(queries)
        key_nums, query_nums = self._get_nums(keys), self._get_nums(queries)
        keys, queries = self._lemmatize_eng_words(keys), self._lemmatize_eng_words(queries)
        key_lemmas, query_lemmas = self._lemmatize_rus_words(keys), self._lemmatize_rus_words(queries)
        return key_lemmas, query_lemmas, key_nums, query_nums

    _clean_sentence = lambda self, x: re.sub(reg_ex, '', x.lower())
    _clean_from_minus_words = lambda self, x: re.sub(r' -!*\w+', '', x)
    _get_nums = lambda self, x: [list(set(re.findall(r'\w*[\d]\w*', i))) for i in x]
    _punctual_to_space = lambda self, x: re.sub('[,.:;-]', ' ', x)

    def _delete_stop_word(self, sentences_list):
        result_sentences = []
        for sentence in sentences_list:
            sentence = self._clean_from_minus_words(sentence)
            sentence = self._punctual_to_space(sentence)
            sentence = self._clean_sentence(sentence)
            sentence = sentence.split(' ')
            for word in sentence:
                if word in self.stop_words:
                    sentence.remove(word)
            sentence = ' '.join(sentence)
            result_sentences.append(sentence)
        return result_sentences

    def _lemmatize(self, key, query):
        for k_i, k_lemma in enumerate(key):
            for q_i, q_lemma in enumerate(query):
                union_lemmas = set(k_lemma) & set(q_lemma)
                if len(union_lemmas) != 0:
                    union_lemma = union_lemmas.pop()
                    key[k_i] = [union_lemma]
                    query[q_i] = [union_lemma]
        key = set([i[0] for i in key])
        query = set([i[0] for i in query])
        return key, query
