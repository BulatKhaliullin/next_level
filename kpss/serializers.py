from rest_framework import serializers
from .models import KPSSAlgorithmResult
from .exceptions import *
from .kpss_algorithm import KPSSAlgorithm
import pandas as pd


class KPSSAlgorithmResultSerializer(serializers.ModelSerializer):
    ''' This is serializer for KPSSAlgorithmResult model '''

    def validate(self, attrs):
        ''' Here is checking format of the file, for this moment KPSSAlgorithm working only with .csv file format.
            If file format is not .csv than it will give response with exception FileFormatException '''
        file_path = str(attrs['file'].temporary_file_path())
        extension = file_path[file_path.rfind('.') + 1:]
        workbook = pd.read_excel(file_path, sheet_name=attrs['list_name'])
        if not KPSSAlgorithm().check_column_name(attrs['query_column_name'], workbook):
            raise QueryColumnNameException()
        if not KPSSAlgorithm().check_column_name(attrs['key_column_name'], workbook):
            raise KeyColumnNameException()
        if extension != 'csv':
            raise FileFormatException()
        return attrs

    class Meta:
        ''' This serializer depends on KPSSAlgorithmResult model
            Here "file_out" field is read_only, it is giving back after creating KPPSAlgorithmResult model '''
        model = KPSSAlgorithmResult
        fields = ['filename', 'list_name', 'query_column_name', 'key_column_name', 'file', 'file_out']
        read_only_fields = ['file_out']
