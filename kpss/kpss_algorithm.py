import re
import pandas as pd
from .stop_words import get_stop_words
import os
import json
from django.conf import settings
from pattern.en import lemma as pattern_lemma


reg_ex = r'[^\da-zA-Zа-яА-ЯӘҒҚҢӨҰҮҺІҖЁёҗәғқңөұүһі ]+'


class KPSSAlgorithm():
    n_coefficient = 2
    file_path = 'kpss3.xlsx'
    file_out_path = 'out.csv'
    mystem_path = os.path.join(settings.BASE_DIR, 'mystem')
    stop_words_path = os.path.join(settings.BASE_DIR, 'stop_words.xlsx')
    query_column_name = 'Query'
    key_column_name = 'Key'
    html_bidder_out_path = settings.BASE_DIR / 'res.html'

    def __init__(self, sheet_name=None, key_column_name=None, query_column_name=None, file_path=None, file_out_path=None):
        if file_path:
            self.file_path = file_path
        if file_out_path:
            self.file_out_path = file_out_path
        if key_column_name:
            self.key_column_name = key_column_name
        if query_column_name:
            self.query_column_name = query_column_name
        self.workbook = pd.read_csv(self.file_path)
        self.stop_words = get_stop_words(file_path=self.stop_words_path)['СТОП-СЛОВА'].tolist()

    def check_column_name(self, name, workbook=None):
        ''' This func checks if the name exist in columns of uploaded input.csv file
            Return True if the name exist in columns names and False if not '''
        workbook = workbook if workbook is not None else self.workbook
        return name in workbook

    def build_ngramm_bidder_html(self, df):
        bidder = {}
        pivot = {}
        itog = {'Кампания': 'Общий_итог',
                'Потенциал_(показы)': 0, 'Потенциал_(клики)': 0,
                'КПСС_через_показы': 0, 'КПСС_через_клики': 0,
                'Клики': 0, 'Показы': 0, 'Конверсии': 0,
                'Стоимость': 0}
        for i, r in df.iterrows():
            p = pivot.get(r['CampaignName'], {'Кампания': r['CampaignName'],
                                              'Дата': r['Date'],
                                              'Потенциал_(показы)': 0, 'Потенциал_(клики)': 0,
                                              'КПСС_через_показы': 0, 'КПСС_через_клики': 0,
                                              'Клики': 0, 'Показы': 0, 'Конверсии': 0,
                                              'Стоимость': 0})
            clicks = r['Clicks'] if type(r['Clicks']) in [int, float] else 0
            impressions = r['Impressions'] if type(r['Impressions']) in [int, float] else 0
            conversions = r['Conversions'] if type(r['Conversions']) in [int, float] else 0
            revenue = r['Revenue'] if type(r['Revenue']) in [int, float] else 0
            potencial_clicks = (100 - r['coefficient']) / 100 * clicks
            potencial_impressions = (100 - r['coefficient']) / 100 * impressions
            p['Клики'] += clicks
            itog['Клики'] += clicks
            p['Показы'] += impressions
            itog['Показы'] += impressions
            p['Конверсии'] += conversions
            itog['Конверсии'] += conversions
            p['Стоимость'] += revenue
            itog['Стоимость'] += revenue
            p['Потенциал_(показы)'] += potencial_impressions
            itog['Потенциал_(показы)'] += potencial_impressions
            p['Потенциал_(клики)'] += potencial_clicks
            itog['Потенциал_(клики)'] += potencial_clicks
            p['КПСС_через_показы'] += r['coefficient'] * impressions
            p['КПСС_через_клики'] += r['coefficient'] * clicks
            pivot[r['CampaignName']] = p
            for w in r['in_query']:
                b = bidder.get(w, {'Недостающие_слова': w, 'Клики': 0, 'Показы': 0, 'Конверсии': 0, 'Стоимость': 0})
                b['Клики'] += clicks
                b['Показы'] += impressions
                b['Конверсии'] += conversions
                b['Стоимость'] += revenue
                bidder[w] = b
        for p in pivot.values():
            p['КПСС_через_показы'] = p['КПСС_через_показы'] / p['Показы'] if p['Показы'] != 0 else 0
            p['КПСС_через_клики'] = p['КПСС_через_клики'] / p['Клики'] if p['Клики'] != 0 else 0
        pivot_kpps_clicks = [p['КПСС_через_клики'] for p in pivot.values()]
        pivot_kpps_impressions = [p['КПСС_через_показы'] for p in pivot.values()]
        itog['КПСС_через_клики'] = sum(pivot_kpps_clicks) / len(pivot_kpps_clicks)
        itog['КПСС_через_показы'] = sum(pivot_kpps_impressions) / len(pivot_kpps_impressions)
        pivot['Общий_итог'] = itog
        bidder_html = pd.DataFrame(bidder).swapaxes("index", "columns").to_html(index=False, justify='left', classes='sortable')
        pivot_html = pd.DataFrame(pivot).swapaxes("index", "columns").to_html(index=False, justify='left', classes='sortable')
        f = open('res.html', 'w', encoding='utf-8')
        f.write(self._get_bidder_html_pattern(pivot_html, bidder_html))

    def _lemmatize_eng_words(self, sentences_list):
        result_sentences = []
        for sentence in sentences_list:
            new_sentence = []
            for word in sentence.split(' '):
                try:
                    new_sentence.append(pattern_lemma(word))
                except Exception:
                    new_sentence.append(pattern_lemma(word))
            result_sentences.append(' '.join(new_sentence))
        return result_sentences

    def _lemmatize_rus_words(self, sentences_list, step=5000):
        result_lemmas = []
        try:
            for i in range(0, len(sentences_list), step):
                if i + step < len(sentences_list):
                    sentences_string = ' IS '.join(sentences_list[i:i + step])
                else:
                    sentences_string = ' IS '.join(sentences_list[i:len(sentences_list)])
                stream = os.popen(f'echo "{sentences_string}" | {self.mystem_path} -e UTF-8 --format json')
                cons_res = json.loads(stream.read())

                sentences_lemmas = []
                for item in cons_res:
                    if item['analysis']:
                        sentences_lemmas.append([i['lex'] for i in item['analysis']])
                    elif item['text'] != 'IS':
                        sentences_lemmas.append([item['text']])
                    else:
                        result_lemmas.append(sentences_lemmas)
                        sentences_lemmas = []
                # print(f'Last: {sentences_lemmas}')
                result_lemmas.append(sentences_lemmas)
        except Exception as e:
            result_lemmas = self._lemmatize_rus_words(sentences_list, step//2)
        return result_lemmas

    def get_coefficient_csv_file(self):
        queries = self.workbook[self.query_column_name]
        keys = self.workbook[self.key_column_name]
        rows_count = len(queries)
        none_list = [None]*rows_count
        df = pd.DataFrame(data={'query': queries, 'key': keys, 'query_lemmas': none_list, 'key_lemmas': none_list,
                                'key_nums': none_list, 'query_nums': none_list,
                                'unique': none_list, 'in_key': none_list, 'in_query': none_list, 'coefficient': none_list})
        df['key_lemmas'], df['query_lemmas'], df['key_nums'], df['query_nums'] = self._normalize(df['key'], df['query'])
        for i, r in df.iterrows():
            key_lemmas, query_lemmas = self._lemmatize(r['key_lemmas'] + r['key_nums'], r['query_lemmas'] + r['query_nums'])
            unique_lemmas = len(key_lemmas.union(query_lemmas))
            in_query = len(query_lemmas - key_lemmas)
            in_key = len(key_lemmas - query_lemmas)
            r['unique'], r['in_key'], r['in_query'] = key_lemmas.union(query_lemmas), key_lemmas - query_lemmas, query_lemmas - key_lemmas
            r['coefficient'] = round((unique_lemmas - in_key - in_query) / (unique_lemmas + in_key * self.n_coefficient) * 100)
        self.build_ngramm_bidder_html(pd.concat([self.workbook, df], axis=1, join='inner'))
        self.workbook['КПСС'] = df['coefficient']
        self.workbook = self.workbook.drop(['Unnamed: 0'], axis=1)
        self.workbook.to_csv(self.file_out_path, encoding='utf-8')

        df.to_csv('entry.csv', encoding='utf-8')

    def _normalize(self, keys, queries):
        keys, queries = self._delete_stop_word(keys), self._delete_stop_word(queries)
        key_nums, query_nums = self._get_nums(keys), self._get_nums(queries)
        keys, queries = self._lemmatize_eng_words(keys), self._lemmatize_eng_words(queries)
        key_lemmas, query_lemmas = self._lemmatize_rus_words(keys), self._lemmatize_rus_words(queries)
        return key_lemmas, query_lemmas, key_nums, query_nums

    _clean_sentence = lambda self, x: re.sub(reg_ex, '', x.lower())
    _clean_from_minus_words = lambda self, x: re.sub(r' -!*\w+', '', x)
    _get_nums = lambda self, x: [list(set(re.findall(r'\w*[\d]\w*', i))) for i in x]
    _punctual_to_space = lambda self, x: re.sub('[,.:;-]', ' ', x)

    def _delete_stop_word(self, sentences_list):
        result_sentences = []
        for sentence in sentences_list:
            sentence = self._clean_from_minus_words(sentence)
            sentence = self._punctual_to_space(sentence)
            sentence = self._clean_sentence(sentence)
            sentence = sentence.split(' ')
            for word in sentence:
                if word in self.stop_words:
                    sentence.remove(word)
            sentence = ' '.join(sentence)
            result_sentences.append(sentence)
        return result_sentences

    def _get_lemmas(self, word):
        stream = os.popen(f'echo "{word}" | {self.mystem_path} -e UTF-8 --format json')
        cons_res = json.loads(stream.read())
        cons_res = cons_res[0]['analysis'] if cons_res else []
        if cons_res:
            lemmas = {i['lex'] for i in cons_res}
        else:
            lemmas = {pattern_lemma(word)}
        return list(lemmas)

    def _lemmatize(self, key, query):
        for k_i, k_lemma in enumerate(key):
            for q_i, q_lemma in enumerate(query):
                union_lemmas = set(k_lemma) & set(q_lemma)
                if len(union_lemmas) != 0:
                    union_lemma = union_lemmas.pop()
                    key[k_i] = [union_lemma]
                    query[q_i] = [union_lemma]
        key = set([i[0] for i in key])
        query = set([i[0] for i in query])
        return key, query

    def _get_bidder_html_pattern(self, df1, df2):
        return '<!DOCTYPE html>\n<meta charset="UTF-8">\n<html>\n<head>\n<title> Сводка </title>\n' \
               '<script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>\n' \
               '<style>body {display: flex;\nmargin: 0;\n}\n' \
               'section {display: inline-block;\nheight: 100vh;\noverflow-x: auto;\nfont-size: 2vmin;\nword-wrap: break-word;\n}\n' \
               'section:nth-child(1) {\nbackground-color: white;\nwidth: 65%;\n}\n' \
               'section:nth-child(2) {\nbackground-color: beige;\nwidth: 35%;\n}\n' \
               f'</style>\n</head>\n<body>\n<section>\n{df1}\n</section>\n<section>\n{df2}\n</section>\n</body>'
