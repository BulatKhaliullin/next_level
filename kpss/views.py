from rest_framework.generics import CreateAPIView
from .serializers import KPSSAlgorithmResultSerializer
from .models import KPSSAlgorithmResult


class KPSSAlgorithmView(CreateAPIView):
    queryset = KPSSAlgorithmResult.objects.all()
    serializer_class = KPSSAlgorithmResultSerializer
