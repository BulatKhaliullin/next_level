import pandas as pd


def get_stop_words(file_path='stop_words.xlsx'):
    workbook = pd.read_excel(file_path)
    return workbook
