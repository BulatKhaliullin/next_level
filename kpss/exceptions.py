from rest_framework.exceptions import APIException


class FileFormatException(APIException):
    status_code = 499
    default_detail = 'Please upload csv format file'
    default_code = 'wrong_file_format'


class QueryColumnNameException(APIException):
    status_code = 499
    default_detail = 'Please write right name of query column'
    default_code = 'wrong_query_column_name'


class KeyColumnNameException(APIException):
    status_code = 499
    default_detail = 'Please write right name of key column'
    default_code = 'wrong_key_column_name'
