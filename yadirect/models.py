from django.db import models


class Account(models.Model):
    name = models.CharField(max_length=200)
    access_token = models.CharField(max_length=1000)
    login = models.CharField(max_length=1000)


class Profile(models.Model):
    name = models.CharField(max_length=200)
    agency_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='profiles', blank=True, null=True)
    goal_name = models.CharField(max_length=500, default='Ecommerce: покупка')

    def __str__(self):
        return self.name
