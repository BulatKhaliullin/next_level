from .models import *
from tapi_yandex_direct import YandexDirect
import pandas as pd
from kpss.models import *
from kpss.ngramm_algorithm import NgrammBidderAlgorithm
import json, requests


class YaDirect():
    def __init__(self, access_token, login, agency_account):
        self.client = YandexDirect(access_token=access_token, login=login)
        self.access_token = access_token
        self.agency_account = agency_account

    def load_profiles(self):
        """ Making query for getting clients of agency account and saving them in DB """
        body = {'method': 'get', 'params': {'SelectionCriteria': {}, 'FieldNames': ['Login']}}
        agency_clients = self.client.agencyclients().post(data=body)
        profiles = [Profile(name=a['Login'], agency_account=self.agency_account) for a in agency_clients['result']['Clients']]
        Profile.objects.filter(agency_account=self.agency_account).delete()
        Profile.objects.bulk_create(profiles, batch_size=100000)

    def load_report(self, profile_login):
        profile_client = YandexDirect(access_token=self.access_token, login=profile_login)
        b = {'params': {'SelectionCriteria': {}, 'FieldNames': ['Query', 'Date', 'Criterion', 'Impressions', 'Clicks',
                                                                'Cost', 'Conversions', 'Revenue', 'CampaignName'],
             'ReportName': 'ActualDataV3', 'ReportType': 'SEARCH_QUERY_PERFORMANCE_REPORT',
             'DateRangeType': 'LAST_WEEK', 'Format': 'TSV', 'IncludeVAT': 'YES', 'IncludeDiscount': 'YES'}}
        reports = profile_client.reports().post(data=b)
        out_path = self.tsv_to_csv(reports.data, profile_login)
        if out_path:
            kpss_result = KPSSAlgorithmResult.objects.create(filename=f'final_{profile_login}',
                                                             query_column_name='Query',
                                                             key_column_name='Criterion',
                                                             file=out_path)
            kpss_result.save()
            return kpss_result.file_out.path
        return out_path

    def load_report_for_ngramm(self, profile_login):
        profile_client = YandexDirect(access_token=self.access_token, login=profile_login)
        campaign_ids, goal_ids = self.get_campaigns_goals(profile_login, profile_client)
        body = {'params': {'SelectionCriteria': {}, 'FieldNames': ['Query', 'Impressions', 'Clicks', 'Cost',
                                                                   'Conversions', 'Revenue', 'CampaignName'],
                           'Goals': list(goal_ids),
                           'ReportName': 'NgrammDatav2', 'ReportType': 'SEARCH_QUERY_PERFORMANCE_REPORT',
                           'DateRangeType': 'LAST_WEEK', 'Format': 'TSV', 'IncludeVAT': 'YES', 'IncludeDiscount': 'YES'}}
        reports = profile_client.reports().post(data=body)
        out_path = self.tsv_to_csv(reports.data, 'for_ngramm_' + profile_login)
        if out_path:
            ngramm_bidder = NgrammBidderAlgorithm(file_path=out_path, account=self.agency_account,
                                                  profile_login=profile_login, campaign_ids=campaign_ids,
                                                  yd=profile_client)
            ngramm_bidder.build_file()
        return out_path

    def tsv_to_csv(self, tsv, profile_login):
        listed_tsv = [i.split('\t') for i in tsv.split('\n') if i]
        if len(listed_tsv) > 1:
            df = pd.DataFrame(listed_tsv[1:], columns=listed_tsv[0])
            out_path = f'./static/reports/{profile_login}.csv'
            df.to_csv(out_path, encoding='utf-8')
            return out_path
        else:
            return None

    def get_campaigns_goals(self, profile_login, yd):
        profile_goal = Profile.objects.filter(agency_account=self.agency_account, name=profile_login).first().goal_name
        campaigns = yd.campaigns().post(data={'method': 'get', 'params': {'FieldNames': ['Id']}})
        campaign_ids = [i['Id'] for i in campaigns.data['result']['Campaigns']]
        goal_ids = set()
        for i in range(0, len(campaign_ids), 100):
            data = {'method': 'GetStatGoals', 'token': self.access_token, 'locale': 'ru',
                    'param': {'CampaignIDS': campaign_ids[i: i+100]}}
            jdata = json.dumps(data, ensure_ascii=False).encode('utf8')
            resp = requests.post('https://api.direct.yandex.ru/live/v4/json/', jdata)
            [goal_ids.add(i['GoalID']) for i in resp.json()['data'] if i['Name'] == profile_goal] # 'Ecommerce: добавление в корзину']
        return campaign_ids, goal_ids
