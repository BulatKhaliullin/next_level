from django.apps import AppConfig


class YadirectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'yadirect'
