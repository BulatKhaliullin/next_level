# Generated by Django 4.1 on 2022-12-05 16:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yadirect', '0004_alter_account_login'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='goal_name',
            field=models.CharField(default='Ecommerce: покупка', max_length=500),
        ),
    ]
