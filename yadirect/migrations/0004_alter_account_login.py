# Generated by Django 4.1 on 2022-10-19 22:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yadirect', '0003_rename_token_account_access_token_account_login'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='login',
            field=models.CharField(max_length=1000),
        ),
    ]
